import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { User } from "../domain";

type AuthState = {
	user: User | null,
	token: string | null,
};

const initialState: AuthState = {
	user: null,
	token: null,
};

export const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		login: (state, action: PayloadAction<AuthState>) => {
			state.user = action.payload.user;
			state.token = action.payload.token;
		},

		logout: (state) => {
			state.user = null;
			state.token = null;
		},

		updateProfile: (state, action: PayloadAction<Partial<User>>) => {
			if(!state.user)
				return;

			state.user = {
				...state.user,
				...action.payload,
			};
		},
	},
})

// Action creators are generated for each case reducer function
export const { login, logout, updateProfile } = authSlice.actions;

export default authSlice.reducer;
