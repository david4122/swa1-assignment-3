import Game from "./domain/game";
import User from "./domain/user";

export type { User, Game };
