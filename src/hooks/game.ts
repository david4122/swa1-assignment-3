import { useCallback, useEffect, useState } from "react";
import { Game } from "../domain";
import env from "../environment";
import { okOrReject } from "../utils";
import { useAppSelector } from "./hooks";
import { useAuthedFetch } from "./users";

export function useGame() {
	const authedFetch = useAuthedFetch();
	const user = useAppSelector(s => s.auth.user?.id);
	const [game, setGame] = useState<Game | null>(null);
	const [error, setError] = useState<string | null>(null);

	const updateGame: (updated: Partial<Game>) => Promise<void> = useCallback((updated: Partial<Game>) => {
		if(!game)
			return Promise.reject('Game not started');

		return authedFetch(`${env.apiBaseUrl}/games/${game.id}`, {
			method: 'PATCH',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(updated),
		})
			.then(okOrReject)
			.then(() => setGame(g => g ? { ...g, ...updated } : null))
			.catch(err => setError(err));
	}, [authedFetch, game, setGame]);

	const updateScore = useCallback(
		(score: number) => updateGame({ score }),
		[updateGame]);

	const completeGame = useCallback(
		() => updateGame({ completed: true }),
		[updateGame]);

	useEffect(() => {
		if(!user || game)
			return;

		authedFetch(`${env.apiBaseUrl}/games`, {
			method: 'POST',
			body: JSON.stringify({ user }),
		})
			.then(okOrReject)
			.then(r => r.json())
			.then(g => setGame(g))
			.catch(err => setError(err));
	}, [authedFetch, user, game]);

	return { game, error, updateScore, completeGame };
}

export function useHighScores(userId?: string) {
	const [games, setGames] = useState<Game[] | null>(null);
	const [error, setError] = useState<string | null>(null);
	const authedFetch = useAuthedFetch();

	useEffect(() => {
		authedFetch(`${env.apiBaseUrl}/games`)
			.then<Game[]>(r => r.json())
			.then(gs => gs
				.filter(g => g.completed)
				.sort((g1, g2) => g2.score - g1.score)
				.slice(0, 10))
			.then(gs => setGames(gs))
			.catch(err => setError(err));
	}, [authedFetch]);

	return { games, error };
}
