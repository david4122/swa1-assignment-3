import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import App from './App';
import './index.css';
import LoginPage from './page/LoginPage';
import RegisterPage from './page/RegisterPage';
import store from './redux/store';
import reportWebVitals from './reportWebVitals';
import ProfilePage from './page/ProfilePage';
import PlayPage from './page/PlayPage';
import HighScoresPage from './page/HighScoresPage';

const router = createBrowserRouter([
	{
		path: '/',
		element: <App />,
		children: [
			{
				path: 'login',
				element: <LoginPage />
			},
			{
				path: 'register',
				element: <RegisterPage />
			},
			{
				path: 'profile',
				element: <ProfilePage />
			},
			{
				path: 'highscores',
				element: <HighScoresPage />
			},
			{
				path: 'play',
				element: <PlayPage />
			},
		]
	}
]);

const root = ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement
);
root.render(
	<React.StrictMode>
		<Provider store={store}>
			<RouterProvider router={router} />
		</Provider>
	</React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
