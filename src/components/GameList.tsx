import { Stack } from "react-bootstrap";
import { Game } from "../domain";
import GameTile from "./GameCard";

export type GameListProps = {
	games: Game[] | null,
};

export default function GameList({ games }: GameListProps) {
	const gameTiles = games?.map(g =>
		<GameTile
			key={g.id}
			game={g} />);

	return (
		<Stack gap={3}>
			{gameTiles?.length === 0 ? 'No games' : gameTiles}
		</Stack>
	);
}
