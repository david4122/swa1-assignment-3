import { Button, Stack } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useLogout, useUser } from "../hooks/users";

export default function ProfileButton() {
	const { user } = useUser();
	const logout = useLogout();
	const nav = useNavigate();

	if(user) {
		return (
			<Stack direction="horizontal" gap={2}>
				<Button onClick={() => nav('/profile')}>{user.username}</Button>
				<Button onClick={() => logout()}>Logout</Button>
			</Stack>
		);
	} else {
		return (
			<Stack direction="horizontal" gap={2}>
				<Button onClick={() => nav('/login')}>Login</Button>
				<Button onClick={() => nav('/register')}>Register</Button>
			</Stack>
		);
	}

}
