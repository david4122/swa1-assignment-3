import { Alert, Col, Container, Row } from "react-bootstrap";
import UserDataForm from "../components/UserDataForm";
import { User } from "../domain";
import { useRequireAuthed, useUser } from "../hooks/users";

export default function ProfilePage() {
	useRequireAuthed();

	const { user, error, updateUser } = useUser();

	const handleUpdate = (updated: Partial<User>) => {
		updateUser(updated);
	};

	return (
		<Container>
			{error && <Row>
				<Col>
					<Alert variant="danger">{error}</Alert>
				</Col>
			</Row>}

			<Row>
				<Col>
					<UserDataForm
						header="Update Profile"
						onSubmit={handleUpdate}
						submitLabel="Update"
						user={user} />
				</Col>
			</Row>
		</Container>
	);
}
