import { useState } from "react";
import { Alert, Col, Container, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import UserDataForm from "../components/UserDataForm";
import { UserData, useCreateUser } from "../hooks/users";

export default function RegisterPage() {
	const createUser = useCreateUser();
	const nav = useNavigate();
	const [error, setError] = useState<string | null>(null);

	const handleSubmit = (e: UserData) => {
		createUser(e)
			.then(() => nav('/login'))
			.catch(err => setError(err));
	};

	return (
		<Container>
			{error && <Row>
				<Col>
					<Alert variant="danger">{error}</Alert>
				</Col>
			</Row>}

			<Row>
				<Col>
					<UserDataForm header="Register" submitLabel="Register" onSubmit={handleSubmit} />
				</Col>
			</Row>
		</Container>
	);
}
