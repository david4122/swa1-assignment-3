import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import GameList from "../components/GameList";
import { useHighScores } from "../hooks/game";
import { useRequireAuthed } from "../hooks/users";

export default function HighScoresPage() {
	useRequireAuthed();

	const nav = useNavigate();
	const { games, error } = useHighScores();

	const handleCreateGame = () => nav('/play');

	return (
		<Container className="d-flex flex-column gap-3">
			{error && <Row>
				<Col>
					<Alert variant="danger">{error}</Alert>
				</Col>
			</Row>}

			<Row>
				<Col>
					<Button onClick={handleCreateGame}>New game</Button>
				</Col>
			</Row>

			<Row>
				<Col>
					<GameList games={games}  />
				</Col>
			</Row>
		</Container>

	);
}
